﻿
var Debug = {
    Levels: {
        INFO: 0,
        LOG: 1,
        WARN: 2,
        ERROR: 3
    },
    Level: 0,
    Console: {
    	Status: function(category, field, value)
    	{
    		if ($.Hurricane.Debug.Status == null)
    		{
    			$.Hurricane.Debug.InitStatus($.Hurricane.Config.statusSelector);
    		}
    		var Category = $.Hurricane.Debug.Status.find('.' + category);
    		if (Category.length == 0)
    		{
    			// Create category div.
    			Category = $('<div/>').addClass(category);
    			$.Hurricane.Debug.Status.append(Category);
    		}
    		
    		var Field = Category.find('.' + field);
    		if (Field.length == 0)
    		{
    			// Create field div.
    			Field = $('<div/>').addClass(field);
    			Category.append(Field);
    		}
    		
    		// Set field value.
    		Field.html(category + " - " +  field + ":" + value);
    		
    	},
        Log: function (message, severity, opts) {
            message = "HURRICANEJS:" + message;
            opts = opts || "";
            if (severity < $.Hurricane.Debug.Level) return;
            switch (severity) {
                case 0:
                    console.info(message, opts);
                    break;
                case 1:
                    console.log(message, opts);
                    break;
                case 2:
                    console.warn(message, opts);
                    break;
                case 3:
                    console.error(message, opts);
                    break;
                default:
                    console.log(message, opts);
                    break;
            }
        }

    },
    Status: null,
    InitStatus: function(selector)
    {
    	this.Status = $(selector);
    }
};


function assert(exp, message) {
    if (!exp) {
        throw new AssertException(message);
    }
}
function AssertException(message) { this.message = message; }
AssertException.prototype.toString = function () {
    return 'AssertException: ' + this.message;
}


$.Hurricane.Debug = Debug;
var $HD = $.Hurricane.Debug;
