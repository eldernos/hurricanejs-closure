/*
 * STATIC CLASS.
 * 
 */
var ObjectManager = {
	Init: function(world)
	{
		this.Index = new QuadTree(world.size);
		$(this).trigger('Initialized');
	},
	Update: function(lapsed) {
		
	}
};
$.Hurricane.ObjectManager = ObjectManager;



var PositionedObject = HBase.extend({
	_className: "PositionedObject",
	// Fields
	id: null,
	position: null,  	// Vector 3
	rotation: null,
	size: null,		 	// Vector 3
	velocity: null,  	// Vector 3
	acceleration: null,	// Vector 3
	assetId: 0,  		// Int.  
	representation: null,
	layer: 0,
	
	attachedObject: null,
	attachedAxes: null,
	
	attributes: null,
	
	
	// Events
	moved: null,
	onMoved: function(previousPosition) {
		this.moved.trigger(new EventArgs({previousPosition: previousPosition}));	
	},
		
	// Constructor
    ctor: function (id, position, size, velocity, acceleration, assetId, layer) {
    	this.moved = new HEvent(this, 'Moved');
    	this.id = id || $H.Utility.newGuid();
    	this.position = position || new $3.Vector3(0, 0, 0);
    	this.size = size || new $3.Vector3(1, 1, 0);
    	this.velocity = velocity || new $3.Vector3(0, 0, 0);
    	this.acceleration = acceleration || new $3.Vector3(0, 0, 0);
    	this.assetId = assetId || 0;
    	this.layer = layer || 0;
    	this.rotation = new $3.Vector3(0, 0, 0);
    	if (this.assetId != 0)
    	{
    		this.representation = $H.Graphics.AssetManager.GetSprite(this.assetId);
    	}
    	
    	this.attributes = new Array();
    	
    },
    
    // Methods
    Move: function(vector) {
    	var prev = this.position.clone();
		this.position.addSelf(vector);
		this.onMoved(prev);
		
	},
	Rotate: function(rotation) {
		this.prev = this.rotation.clone();
		this.rotation.addSelf(rotation);
		this.onMoved(prev);
	},
    AttachTo: function(positionedObject, axes)
    {
    	this.attachedObject = positionedObject;
    	this.attachedAxes = axes;
    	
    	var relativeVector = new $3.Vector3(0, 0, 0);    
    	if ('x' in this.attachedAxes) relativeVector.x = positionedObject.position.x - this.position.x;
    	if ('y' in this.attachedAxes) relativeVector.y = positionedObject.position.y - this.position.y;
    	if ('z' in this.attachedAxes) relativeVector.z = positionedObject.position.z - this.position.z;
    	
    	this.Move(relativeVector);
    	positionedObject.moved.bind(this, this.TargetMoved);	    	
    },
    TargetMoved: function(sender, eventArgs)
    {
    	var relativeVector = new $3.Vector3(0, 0, 0);
    	
    	if ('x' in this.attachedAxes) relativeVector.x = sender.position.x - this.position.x;
    	if ('y' in this.attachedAxes) relativeVector.y = sender.position.y - this.position.y;
    	if ('z' in this.attachedAxes) relativeVector.z = sender.position.z - this.position.z;
    	
    	this.Move(relativeVector);
    }
});


var World = PositionedObject.extend({
	// Fields
	_className: "World",
	
	// Constructor
	ctor: function(size)
	{
		size = size || new $3.Vector3(100, 100);
		//this._$.ctor(false, false, size);
		this._super(false, false, size);
	}
	// Methods
});
