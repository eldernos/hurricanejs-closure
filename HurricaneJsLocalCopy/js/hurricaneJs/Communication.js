﻿var Communication = {
    init: function () {
        $.Hurricane.include("js/jquery.signalR.min");
        $.ajax({
            url: "/Hurricane/signalr/hubs",
            dataType: "script",
            async: false,
            success: function () {
                console.log('SignalR Hubs Loaded');
            }

        });
        $.Hurricane.server = $.connection.socketServer;
        $.Hurricane.server.navigate = function (data) {
            $.Hurricane.viewport.navigate(data);
        },
            this.server.instruction = function (data) {
            var instruction = JSON.parse(data);
            //console.info(instruction);
            $.Hurricane.ObjectManager.operations[instruction[0]](instruction);
            //console.log(data);
            //Hurricane.Instance.operations[instruction[0]](instruction);
        };
        $.connection.hub.start(function () {
            $.Hurricane.server.gameId = $.Hurricane.config.gameId;
            //$.Hurricane.server.connect(JSON.stringify({ username: "username", password: "password" }));
            $.Hurricane.server.connect(JSON.stringify({}));
        });
    },
    send: function (message) {
        $.Hurricane.server.input(message);
    }
};

$.Hurricane.Communication = Communication;
