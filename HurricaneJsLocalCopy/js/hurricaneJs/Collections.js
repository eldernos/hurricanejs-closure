﻿/*
var SortableDictionary = HBase.extend({
    init: function (getIndex) {
        if (getIndex) {
            this.getIndex = getIndex;
        }
    },
    getIndex: function (item) {
        return item;
    },
    store: [],
    sort: function () {
        store = store.sort($.proxy(function (a, b) {
            return this.getIndex(a) - this.getIndex(b);
        }), this);
    },
    add: function (item) {
        this.store.push(item);
    },
    remove: function (item) {
        delete this.store[this.store.indexOf(item)];
    },
    contains: function (item) {
        return this.store.indexOf(item) > -1;
    }

});


//if (!$.Hurricane) throw "Hurricane Not Defined in Collections.js";
//http://perfectionkills.com/how-ecmascript-5-still-does-not-allow-to-subclass-an-array/
// http://www.bennadel.com/blog/2292-Extending-JavaScript-Arrays-While-Keeping-Native-Bracket-Notation-Functionality.htm
// Define the collection class.
/*
var HArrayBase = (function () {


    // I am the constructor function.
    function Collection() {

        // When creating the collection, we are going to work off
        // the core array. In order to maintain all of the native
        // array features, we need to build off a native array.
        var collection = Object.create(Array.prototype);

        // Initialize the array. This line is more complicated than
        // it needs to be, but I'm trying to keep the approach
        // generic for learning purposes.
        collection = (Array.apply(collection, arguments) || collection);

        // Add all the class methods to the collection.
        Collection.injectClassMethods(collection);

        // Return the new collection object.
        return(collection);

    }


    // ------------------------------------------------------ //
    // ------------------------------------------------------ //


    // Define the static methods.
    Collection.injectClassMethods = function (collection) {

        // Loop over all the prototype methods and add them
        // to the new collection.
        for (var method in Collection.prototype) {

            // Make sure this is a local method.
            if (Collection.prototype.hasOwnProperty(method)) {

                // Add the method to the collection.
                collection[method] = Collection.prototype[method];

            }

        }

        // Return the updated collection.
        return(collection);

    };


    // I create a new collection from the given array.
    Collection.fromArray = function (array) {

        // Create a new collection.
        var collection = Collection.apply(null, array);

        // Return the new collection.
        return(collection);

    };


    // I determine if the given object is an array.
    Collection.isArray = function (value) {

        // Get it's stringified version.
        var stringValue = Object.prototype.toString.call(value);

        // Check to see if the string represtnation denotes array.
        return(stringValue.toLowerCase() === "[object array]");

    };


    // ------------------------------------------------------ //
    // ------------------------------------------------------ //


    // Define the class methods.
    Collection.prototype = {
        count: function () {
            var count = 0, key;
            for (key in this) {
                if (this.hasOwnProperty(key)) count++;
            }
            return count;
        }
        /*
    // I add the given item to the collection. If the given item
    // is an array, then each item within the array is added
    // individually.
        add: function (value) {

            // Check to see if the item is an array.
            if (Collection.isArray(value)) {

                // Add each item in the array.
                for (var i = 0 ; i < value.length ; i++) {

                    // Add the sub-item using default push() method.
                    Array.prototype.push.call(this, value[i]);

                }

            } else {

                // Use the default push() method.
                Array.prototype.push.call(this, value);

            }

            // Return this object reference for method chaining.
            return(this);

        },


        // I add all the given items to the collection.
        addAll: function () {

            // Loop over all the arguments to add them to the
            // collection individually.
            for (var i = 0 ; i < arguments.length ; i++) {

                // Add the given value.
                this.add(arguments[i]);

            }

            // Return this object reference for method chaining.
            return(this);

        }
        * /
    };


    // ------------------------------------------------------ //
    // ------------------------------------------------------ //
    // ------------------------------------------------------ //
    // ------------------------------------------------------ //


    // Return the collection constructor.
    return(Collection);


}).call({});



/*
function HArrayBase() {
    var _array = Object.create(Array.prototype);
    _array = (Array.apply(_array, arguments) || _array);
    HArrayBase.injectClassMethods(_array);
    return (_array);

}
HArrayBase.injectClassMethods = function (collection) {
    for (var method in HArrayBase.prototype) {
        if (HArrayBase.prototype.hasOwnProperty(method)) {
            collection[method] = HArrayBase.prototype[method];
        }
    }
    return (collection);
};
HArrayBase.fromArray = function (array) {
    var collection = HArrayBase.apply(null, array);
    return (collection);

};
HarrayBase.isArray = function (value) {
    var stringValue = Object.prototype.toString.call(value);
    return (stringValue.toLowerCase() === "[object array]");
};

//var HArrayBase = PClass.extend(new Array);
var HArray = HArrayBase.extend({
    init: function () {

    },
    array: []

});
var HHashTable = HArrayBase.extend({
    init: function () {
    }
});
var HDictionary = HArrayBase.extend({
    init: function (indexType, valueType) {

    },
    indexType: "",
    valueType: ""
});
var HQueue = HArrayBase.extend({
    init: function () {

    }
});
var HStack = HArrayBase.extend({
    init: function () {

    }
});

*/