﻿var Navigation = {
	currentScreen : {},
	route : function(screen) {
		if(this.currentScreen instanceof Screen) {
			this.currentScreen.stop();
			this.currentScreen.cleanup();
		}
		this.currentScreen = screen;
		this.currentScreen.load();
		this.currentScreen.start();
	},
	showDialog : function(dialog) {

	}
};
$.Hurricane.Navigation = Navigation;

var Screen = HBase.extend({
	_className: "Screen",
	screenData : null,
	ctor : function(screenData) {
		this.screenData = screenData;
	},
	load : function() {

	},
	start : function() {

	},
	cleanup : function() {

	},
	stop : function() {

	},
	update : function() {

	}
});

var Html = Screen.extend({
	_className: "Html",
	ctor : function() {

	}
});
var WorldView = Screen.extend({
	_className: "WorldView",
	timer : {},
	ctor : function(screenData) {
		this._super(screenData);

	},
	load : function() {
		//timer = setInterval(this.update, 50);
		// 1 - Load All Assets
		var that = this;
		$($.Hurricane.Graphics.AssetManager).bind('Ready', function() {
			// 2 - Initialize starting scene po's.
			
			that.screenData.objects().forEach(function(po, index, array) {
				$.Hurricane.Graphics.Register(po);
			});
			setInterval(function() {
				$.Hurricane.Graphics.Draw();
			}, 33);
				
			// 3 - Clear and Render Scene to Graphics
		});
		$.Hurricane.Graphics.AssetManager.PreLoad(this.screenData.assets);
		
	},
	start : function() {

	},
	update : function(time) {

	}
});
var Dialog = Screen.extend({
	_screenName: "Dialog",
	show : function() {

	},
	hide : function() {

	}
});
