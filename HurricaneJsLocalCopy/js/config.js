﻿/*
 $.Hurricane.config =
 {
 "socketServerUrl": "ws://192.168.1.22/Hurricane/Services/HurricaneServer.ashx",
 "assetPath": "http://localhost/Hurricane/Asset.ashx",
 "gameId": "a58de6c0-7501-4086-b125-2701367872eb",
 debuggingEnabled: true
 }*/
$.Hurricane.Config = {
	
	viewport : {
		element : '#canvas',
		width : 1000,
		height : 1000,
		world : new World(new $3.Vector3(100, 100))
	},
	local : true,
	assetPath : "images/",
	assetExt : "png",
	debuggingEnabled : true,
	statusSelector : '#status',
	load : function() {
		$.Hurricane.Navigation.route((function() {
			var screen = new WorldView({
				assets : [1000000, 1000001, 1000002, 1000003, 1000004, 1000005, 1000006, 1000007],
				objects : function() {
					var width = 100;
					var height = 100;
					var array = new Array();
					for( x = 0; x < width; x++) {
						for( y = 0; y < height; y++) {
							var sprite = new PositionedObject(false, new $3.Vector3(x, y, 0), false, false, false, 1000005);
							sprite.attributes["speed"] = 0;
							sprite.attributes["name"] = "Desert";
							array.push(sprite);
						}
					}
					var spr = new PositionedObject(false, new $3.Vector3(5, 5, 0), false, false, false, 1000001, 1);
					spr.attributes["speed"] = 7;
					spr.attributes["name"] = "Character";
					$H.Graphics.viewport.First().displayCamera.AttachTo(spr, {x: 0, y: 0});
					
					array.push(spr);
					return array;
				},
				layers : 2,
				camera : {
					rotation : 0
					
				}

			});
			return screen;
		})());
	},
	update : function() {

	},
	Graphics : {
		PixelScale : 64

	},
	Input: {
		DefaultSelection: "DisplayCamera"
	}
	
}