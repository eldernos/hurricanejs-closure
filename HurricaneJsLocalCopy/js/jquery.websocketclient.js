﻿(function ($) {
    //  Think about being able to tag elements for handling ServerUpdate event handler...  
    $.extend({
        webSocketClient: {
            registry: [],
            connection: null,
            host: "",
            connect: function (host) {
                try {
                    connection = new WebSocket(host);
                    connection.onopen = this.onOpen;
                    connection.onclose = this.onClose;
                    connection.onmessage = this.onMessage;
                    connection.onerror = this.onError;

                }
                catch (ex) {

                }


            },
            disconnect: function () {
                if (connection.readyState != 3) {
                    connection.close();
                }
            },
            test: function () {
                if ("WebSocket" in window) {
                    return true;
                }
                return false;
            },
            onOpen: function (evt) {
                $(document).trigger('webSocketOpened', evt);
            },
            onClose: function (evt) {
                $(document).trigger('webSocketClosed', evt);
            },
            onMessage: function (evt) {
                $(document).trigger('webSocketMessage', evt);
            },
            onError: function (evt) {
                $(document).trigger('webSocketError', evt);
            },
            send: function (message) {
                if (connection.readyState != 1) {
                    return false;
                }
                // Detect if message is object and jsonify it before sending if so... unless just string, then send string.
                try {
                    if (typeof message == 'object') {
                        message = JSON.stringify(message);
                    }
                    connection.send(message);
                }
                catch (ex) {
                    return false;
                }

                return true;
            },
            register: function (channel) {
                // This registers an item to be listening for a SocketUpdate event on a given channel.  For example, an element we want to
                // be listening to move events has an event handler for this move which dicates its behavior when an event of that signature comes
                // in.  
                webSocketClient.registry.push({ key: channel, value: this });
            }
        }
    });
})(jQuery);